# ormuco test

by: Armando Oviedo C.

Pre-Requisites:
Fresh server with Linux CentOS 7.x
Edit file custom_inventory and add the proper IP address (default at: 142.93.69.130)

How to run
ansible-playbook -i custom_inventory deploy_app.yml

Features:
UFW firewall with default incoming rule set to deny and outgoing allowed. The following TCP ports open: 22, 80 and 443.
Nginx, PHP-FPM, MariaDB setup (LEMP stack). No SSL configuration.
Small PHP app on server's root URL / (index.php) on port 80 only.