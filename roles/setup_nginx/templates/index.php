<?php

// ---- Init $_SESSION...

session_start();

// ---- Establish Database Connection...

$database_host = "localhost";
$database_port = "3306";
$database_name = "armovtest";
$database_user = "armovtest";
$database_pass = "ormuco";

$pdo = new PDO("mysql:host=$database_host;
				port=$database_port;
				dbname=$database_name",
				"$database_user",
				"$database_pass"
				);

// ---- If the user submits a form...

if ( $_SERVER["REQUEST_METHOD"] == "POST" ) {

	// $_SESSION user-input placeholders...

	$_SESSION['name']      = $_POST['name'];
	$_SESSION['fav_color'] = $_POST['fav_color'];
	$_SESSION['pet']       = $_POST['pet'];

	// If the user pressed "Remove" in any of the rows...

	if ( isset($_POST['delete']) && isset($_POST['user_id']) ) {
		$sql = "DELETE FROM data WHERE id = :user_id";
		$stmt = $pdo->prepare($sql);
		$stmt->execute(array(
			':user_id' => $_POST['user_id'],
		));
		$_SESSION['delete'] = "Delete successful!";
		header("Location: /");
		die();
	}

	// Input validation for "name" field...

	if ( isset($_POST['name']) ) {

		// 1. It must not be empty...

		if ( $_POST['name'] == "" ) {
			$_SESSION['error'] = "Missing field: name";
			header("Location: /");
			die();
		}

		if ( $_POST['name'] != "" ) {

		// 2. It must be unique, not repeated in the database...

			$stmt = $pdo->query("SELECT name FROM data");

			while ( $row = $stmt->fetch(PDO::FETCH_ASSOC) )  {
     				if ( strcasecmp($row['name'], $_POST['name']) == 0 ) {
					$_SESSION['error'] = "Name is already in the database. Pick another.";
					header("Location: /");
					die();
				}
			}

		}

	}

	//  Input validation for "fav_color": it must not be empty...

	if ( isset($_POST['fav_color']) ) {
		if ($_POST['fav_color'] == "") {
			$_SESSION['error'] = "Missing field: favorite color";
			header("Location: /");
			die();
		}
	}

	//  Input validation for "pet": it must not be empty...

	if ( !isset($_POST['pet']) ) {
		$_SESSION['error'] = "Missing field: favorite pet";
		header("Location: /");
		die();
	}

	//  If all of the above filters were passed, proceed creating the row

	if ( !isset($_SESSION['error']) ) {

		$sql = "INSERT INTO data (name, fav_color, pet) VALUES(:name, :fav_color, :pet)";

		$stmt = $pdo->prepare($sql);

		$stmt->execute( array(
			':name' 	 => $_POST['name'],
			':fav_color' => $_POST['fav_color'],
			':pet' 		 => $_POST['pet'],
		));

		$_SESSION['success'] = "Row inserted!";

		unset($_SESSION['name'], $_SESSION['fav_color'], $_SESSION['pet']);

		header("Location: /");
		die();

	}

}

?>

<?php
	//===============================
	//======= FRONT END START =======
	//===============================
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>

	<div class="intro-wrapper">
		<h1>Personal Preferences Database</h1>
		<p>Insert name, favorite color and pet preference.</p>
	</div>

	<?php // If exists, display the error message ?>

	<?php if ( isset($_SESSION['error']) ): ?>

		<p style="color:red"><?php echo $_SESSION['error'];?></p>
		<?php unset($_SESSION['error']); ?>

	<?php endif; ?>

	<?php // If exists, display the success message ?>

	<?php if ( isset($_SESSION['success']) ): ?>

		<p style="color:green"><?php echo $_SESSION['success'];?></p>
		<?php unset($_SESSION['success']); ?>

	<?php endif; ?>

	<?php // If exists, display the delete success message ?>

	<?php if ( isset($_SESSION['delete']) ): ?>

		<p style="color:green"><?php echo $_SESSION['delete'];?></p>
		<?php unset($_SESSION['delete']); ?>

	<?php endif; ?>

	<?php //---- MAIN FORM ?>

	<form action="/" method="POST">

		<div class="person-name-wrapper">
			<label for="name" style="margin-right: 52px;">Your name:</label>
			<?php // If form fails validation, do not lose the info the user typed, instead show it, then remove the $_SESSION variable holding it. ?>
			<input type="text" id="name" name="name" value="<?php

				if( isset($_SESSION['name']) ) {
					echo htmlentities($_SESSION['name']);
					unset($_SESSION['name']);
				}

			?>">
		</div>

		<div class="favorite-color-wrapper">

			<label for="fav_color">Your favorite color:</label>
			<?php // If form fails validation, do not lose the info the user typed, instead show it, then remove the $_SESSION variable holding it. ?>
			<input type="text" id="fav_color" name="fav_color" value="<?php

				if( isset($_SESSION['fav_color']) ) {
					echo htmlentities($_SESSION['fav_color']);
					unset($_SESSION['fav_color']);
				}

			?>">

		</div>

		<div class="favorite-pet-wrapper">
			<label style="margin-right:12px;">Your favorite pet:</label>
			<?php // If form fails validation, do not lose the info the user typed, instead show it, then remove the $_SESSION variable holding it. ?>
			<input type="radio" name="pet" value="dogs"
				<?php
					if ( isset($_SESSION['pet']) ) {
						if ($_SESSION['pet'] == "dogs") {
							echo "checked";
							unset($_SESSION['pet']);
						}
					}
				?>
			> Dogs
			<input type="radio" name="pet" value="cats"
				<?php
					if ( isset($_SESSION['pet']) ) {
						if ($_SESSION['pet'] == "cats") {
							echo "checked";
							unset($_SESSION['pet']);
						}
					}
				?>
			> Cats
		</div>

		<input type="submit" style="width: 260px; margin: 10px 0">

	</form>

	<?php //---- TABLE INFORMATION FROM THE DATABASE ?>

	<table>

	<tr>
		<th>#</th>
		<th>Name</th>
		<th>Color</th>
		<th>Pet</th>
	</tr>

	<?php

		$stmt = $pdo->query("SELECT * FROM data");

		$counter = 0;

		while ( $row = $stmt->fetch(PDO::FETCH_ASSOC) ) : ?>

			<?php $counter++ ?>

			<tr>
				<td><?php echo $counter; ?></td>
				<td><?php echo $row['name']; ?></td>
				<td><?php echo $row['fav_color']; ?></td>
				<td><?php echo $row['pet']; ?></td>
				<td>
					<?php // This is a form with delete information in each row... ?>
					<form action="/" method="POST">
						<input type="hidden" name="user_id" value="<?php echo $row['id']; ?>">
						<input type="submit" value="Remove" name="delete">
					</form>
				</td>
			</tr>

		<?php endwhile; ?>

	</table>

</body>
</html>
